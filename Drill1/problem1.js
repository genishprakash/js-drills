

const getCarInfoById=(inventory,id)=>{
    
    if(!Array.isArray(inventory) || typeof(id)!="number"){
        return "Please provide the proper input"
    }
    else{
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].id==id){
                return `car ${inventory[index].id} is ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`
            }
        }
        return `No car was found with id:${id} `
    }

}
module.exports=getCarInfoById

    