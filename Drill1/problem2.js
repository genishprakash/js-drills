
const findLastCar=(inventory)=>{
    if(!Array.isArray(inventory)){
        return `Please provide the input in Array`
    }
    if(inventory.length==0){
        return `No cars found`
    }
    return `Last car is ${inventory[inventory.length-1].car_make} ${inventory[inventory.length-1].car_model}`;
}

module.exports=findLastCar