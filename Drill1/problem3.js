const inventory = require("./dataset")

const sortByCarMake=(inventory)=>{
    if(!Array.isArray(inventory)){
        return `Please provide the input in Array`
    }
    if(inventory.length==0){
        return `No cars found`
    }
    for(let currIndex=0;currIndex<inventory.length-1;currIndex++){
        for(let compareIndex=currIndex+1;compareIndex<inventory.length;compareIndex++){
            if(inventory[currIndex].car_make > inventory[compareIndex].car_make){
                let temp=inventory[currIndex]
                inventory[currIndex]=inventory[compareIndex]
                inventory[compareIndex]=temp
            }
        }
    }
    return inventory

}

module.exports=sortByCarMake