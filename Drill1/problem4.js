const findAllTheYears=(inventory)=>{
    let arr=[]
    if(!Array.isArray(inventory)){
        return `Please provide the input in Array`
    }
    if(inventory.length==0){
        return `No cars found`
    }
    for(let index=0;index<inventory.length;index++){
        arr.push(inventory[index].car_year)
    }
    return arr
}
module.exports=findAllTheYears