const findAllTheYears=require("./problem4")
const carsOlderThan2000=(inventory)=>{
    if(!Array.isArray(inventory)){
        return `Please provide the input in Array`
    }
    if(inventory.length==0){
        return `No cars found`
    }
    const years=findAllTheYears(inventory)
    const arr=[]
    for(let year of years){
        if(year>2000){
            arr.push(year)
        }
    }
    if(arr.length==0){
        return `No cars found older than the year 2000`
    }
    return arr
}
module.exports=carsOlderThan2000