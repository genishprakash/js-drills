

const filterBMWAudi =(inventory)=>{

    const arr=[]
    if(!Array.isArray(inventory)){
        return `Please provide the input in Array`
    }
    if(inventory.length==0){
        return `No cars found`
    }
    for(let car of inventory){
        if(car.car_make=="BMW" || car.car_make=="Audi"){
            arr.push(car)
        }
    }
    if(arr.length==0){
        return `No cars found with brand of Audi or BMW`
    }
    return JSON.stringify(arr)
}
module.exports=filterBMWAudi