const getEmailAddresses=(usersDataset)=>{
    const emailDataSet=[];
    if(!Array.isArray(usersDataset)){
        return `Please provide the proper input`;
    }
    if(usersDataset.length==0){
        return `No data's found`;
    }
    for(let user of usersDataset){
        if(!user.email){
            continue;
        }
        
        emailDataSet.push(user.email);
    }
    return emailDataSet;
}

module.exports=getEmailAddresses;