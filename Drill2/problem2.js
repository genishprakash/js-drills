//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

const retrieveHobbiesByAge=(usersDataset,targetAge)=>{

    if(!Array.isArray(usersDataset) ){
        return ` Please provide the proper data`
    }
    if(typeof targetAge!="number"){
        return `Please provide the age in number format`
    }
    if(usersDataset.length==0){
        return `No data found in the dataset`
    }
    let hobbiesOfTargetAge=[]
    for(let user of usersDataset){
        if(user.age==targetAge ){
            if(user.hobbies){
                const userHobbies=`Hobbies of ${user.name}  are ${user.hobbies}`
                hobbiesOfTargetAge.push(userHobbies)
            }
            else{
                const userHobbies=`No hobbies found for ${user.name}`
                hobbiesOfTargetAge.push(userHobbies)
            }
        }
    }
    if(hobbiesOfTargetAge.length==0){
        return `No user found at the target age`
    }
    return hobbiesOfTargetAge
}

module.exports=retrieveHobbiesByAge;