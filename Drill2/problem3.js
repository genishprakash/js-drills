
//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

const getStudentsInAustralia=(usersDataset)=>{
    if(!Array.isArray(usersDataset)){
        return `Please provide proper dataset`
    }
    const users=[];
    for( let user of usersDataset){
        if(user.isStudent && user.country=="Australia"){
            users.push(user.name);
        }
    }
    if(users.length==0){
        return `No Students live in Australia`
    }
    return users;
}

module.exports = getStudentsInAustralia