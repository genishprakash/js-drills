//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

const findUserAtIndex3=(usersDataset)=>{
    if(!Array.isArray(usersDataset)){
        return `Please provide the proper dataset`
    }
    if(usersDataset.length<3){
        return `There is no data at index 3`
    }
    return `The user name is ${usersDataset[2].name} and he lives in  ${usersDataset[2].city}`
}

module.exports=findUserAtIndex3