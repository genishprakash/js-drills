//    Implement a loop to access and print the ages of all individuals in the dataset.

const printAges=(usersDataset)=>{
    if(!Array.isArray(usersDataset)){
        return `Please provide proper dataset`
    }
    if(usersDataset.length==0){
        `No data found`
    }
    for(let user of usersDataset){
        if(!user.age){
            continue
        }
        console.log(`User ${user.name} is a ${user.age} years old person`)
    }
}
module.exports=printAges