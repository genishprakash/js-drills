
//    Create a function to retrieve and display the first hobby of each individual in the dataset.

const retrieveFirstHobby=(usersDataset)=>{
    if(!Array.isArray(usersDataset)){
        return `Please provide the proper dataset`
    }
    if(usersDataset.length==0){
        return `No data found`
    }
    for(let user of usersDataset){
        if(!user.hobbies){
            console.log(`No hobbies found for ${user.name}`)
        }
        console.log(`The first hobby of ${user.name} is ${user.hobbies[0]}`)   
    }

}
module.exports=retrieveFirstHobby