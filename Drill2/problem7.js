//    Write a function that accesses and prints the names and email addresses of individuals aged 25.

const printNamesAndEmailByAge25=(usersDataset)=>{
    if(!Array.isArray(usersDataset)){
        return `Please provide the proper dataset`
    }
    if(usersDataset.length==0){
        return `No data found`
    }
    let isFound=false
    for(let user of usersDataset){
        if(user.age==25){
            isFound=true
            console.log(`The name of the user is ${user.name} . His Email is ${user.email}`)
        }
    }
    if(!isFound){
        console.log(`No users found with the Age of 25`)
    }
}
module.exports=printNamesAndEmailByAge25