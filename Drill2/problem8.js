//    Implement a loop to access and log the city and country of each individual in the dataset.

const printCityAndCountry=(usersDataset)=>{
    if(!Array.isArray(usersDataset)){
        return `Please provide the proper dataset`
    }
    if(usersDataset.length==0){
        return `No data found`
    }
    for(let user of usersDataset){
        if(!user.city || !user.country){
            console.log(`Can't access the city or country for the user : ${user.name}`)
            continue
        }
        console.log(`${user.name} lives in ${user.city},${user.country}`)
    }
}

module.exports=printCityAndCountry