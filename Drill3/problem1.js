// Q1 Find all users who are interested in playing video games.
const usersPlayingGames=(users)=>{
    if(!(typeof users ==='object' && users.constructor === Object)){
        return `Please provide a proper input`
    }
    let usersData={}
    for(let user in users){
    
        if(!users[user].interests ){
            continue;
        }
        if(users[user].interests.length==0){
            continue
        }
        else{
            if(users[user].interests[0].includes("Games")){
                usersData[user]=users[user]
            }
        }
    }
    return usersData
}
module.exports=usersPlayingGames