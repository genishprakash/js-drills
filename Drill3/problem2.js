// Q2 Find all users staying in Germany.

const findUsersInGermany=(users)=>{
    if(!(typeof users ==='object' && users.constructor === Object)){
        return `Please provide a proper input`
    }
    let userInGermany={}
    for(let user in users){
        if(!users[user].nationality){
            continue
        }
        if(users[user].nationality=="Germany"){
            userInGermany[user]=users[user]
        }
    }
    return userInGermany
}

module.exports=findUsersInGermany