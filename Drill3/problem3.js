// /Q3 Find all users with masters Degree.

const userWithMastersDegree=(users)=>{
    if(!(typeof users==='object' && users.constructor=== Object)){
        return ` Please provide a proper input`
    }
    let mastersDegreeHolders={}
    for(let user in users){
        if(!users[user].qualification){
            continue
        }
        else if(users[user].qualification.toLowerCase().includes("masters")){
            mastersDegreeHolders[user]=users[user]
        }
    }
    return mastersDegreeHolders;
}

module.exports=userWithMastersDegree