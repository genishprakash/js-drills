// Q4 Group users based on their Programming language mentioned in their designation.
const groupsOnProgrammingLanguage=(users)=>{
    const groups={}
    const programmingLanguages=['Javascript','Golang','Python']
    if(!(typeof users==='object' && users.constructor=== Object)){
        return ` Please provide a proper input`
    }
    for(let user in users){
        for(let language of programmingLanguages){
            if(users[user].designation.toLowerCase().includes(language.toLowerCase())){
                if(language in groups){
                    groups[language][user]=users[user]
                }
                else{
                    groups[language]={}
                    groups[language][user]=users[user]
                }
            }
        }
    }
    return groups

}

module.exports=groupsOnProgrammingLanguage